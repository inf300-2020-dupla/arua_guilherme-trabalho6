package br.unicamp.ic.inf335;

/**
 * Trabalho 6 - Interação com Banco de Dados
 * 
 * @author Aruã Puertas Costa e Guilherme Kayo Shida
 * 
 */
public class App {
	public static void main(String[] args) {
		Terminal terminal = new Terminal();
		
		terminal.init();
	}
}
