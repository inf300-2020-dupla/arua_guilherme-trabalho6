package br.unicamp.ic.inf335;

import java.util.List;
import java.util.Scanner;

import br.unicamp.ic.inf335.business.Episode;
import br.unicamp.ic.inf335.business.EpisodeController;

/**
 * This class represents a View in the MVC pattern
 * 
 * @author Aruã Puertas Costa e Guilherme Kayo Shida
 *
 */
public class Terminal {
	/**
	 * The controller that controls the application.
	 */
	private EpisodeController controller;

	public Terminal() {
		controller = new EpisodeController();
	}

	/**
	 * Initialize the terminal
	 */
	public void init() {
		System.out.println("*******************************************");
		System.out.println("***** The Simple Friends Episode List *****");
		System.out.println("*******************************************");
		System.out.println("Type a number of a season [1-10] to list the episodes:");

		int season = getInputSeason();

		System.out.println();
		List<Episode> episodes = controller.listEpisodesOfSeason(season);
		
		printEpisodes(season, episodes);
		
		System.out.println();
		System.out.println("Bye!");
	}

	/**
	 * Print the episodes.
	 * 
	 * @param season the season to list.
	 * @param episodes the list with the episodes.
	 */
	private void printEpisodes(int season, List<Episode> episodes) {
		if (!episodes.isEmpty()) {
			System.out.println();
			System.out.println("***** SUCCESS! *****");
			System.out.println("List of all the episodes of season " + season + "\n");
			for (Episode episode : episodes) {
				System.out.println(episode);
			}
		} else {
			System.out.println("We could not find any episodes. Maybe this season does not exist.");
		} 
	}

	/**
	 * Get the season.
	 * 
	 * @return The season from the input.
	 */
	private int getInputSeason() {
		int season;
		Scanner sc = new Scanner(System.in);

		while (!sc.hasNextInt()) {
			System.out.println("The input '" + sc.next() + "' is not valid number! Try Again.");
			System.out.println("Type a number of a season to list the episodes:");
			sc = new Scanner(System.in);
		}
		season = sc.nextInt();
		sc.close();
		
		return season;
	}
}
