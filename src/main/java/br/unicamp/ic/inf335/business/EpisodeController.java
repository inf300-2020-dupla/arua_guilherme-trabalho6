package br.unicamp.ic.inf335.business;

import java.util.List;

import br.unicamp.ic.inf335.data.MongoDbEpisodeRepository;

/**
 * A controller to the application.
 * 
 * @author Aruã Puertas Costa e Guilherme Kayo Shida
 * 
 */
public class EpisodeController {
	/**
	 * The repository that handles the database operations
	 */
	private IEpisodeRepository repository;

	public EpisodeController() {
		repository = new MongoDbEpisodeRepository();
	}
	
	/**
	 * List all episodes
	 * @return 
	 */
	public List<Episode> listAllEpisodes() {
		return repository.getEpisodes();
	}

	/**
	 * List all episodes of a season
	 * 
	 * @param season the season to list the episodes
	 * @return 
	 */
	public List<Episode> listEpisodesOfSeason(int season) {
		return repository.getEpisodesOfSeason(season);
	}
}
