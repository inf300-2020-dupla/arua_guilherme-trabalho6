package br.unicamp.ic.inf335.business;

import java.util.List;

/**
 * Implementing this interface allows us to decouple the business and the data layer.
 * 
 * We can use more than one solution for a database.
 * 
 * When a class implements this interface, it handles operations for episodes. 
 * 
 * @author Aruã Puertas Costa e Guilherme Kayo Shida
 *
 */
public interface IEpisodeRepository {

	/**
	 * Get all episodes from the database.
	 */
	List<Episode> getEpisodes();

	/**
	 * Get all episodes of a season from the database.
	 * 
	 * @param season the number of the season to get all episodes
	 */
	List<Episode> getEpisodesOfSeason(int season);

}