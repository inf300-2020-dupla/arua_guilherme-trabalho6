package br.unicamp.ic.inf335.business;

/**
 * POJO class that represents an Image
 * 
 * @author Aruã Puertas Costa e Guilherme Kayo Shida
 *
 */
public class Image {
	/**
	 * The path to the medium size image.
	 */
	private String medium;
	/**
	 * The path to the original size image.
	 */
	private String original;

	public Image() {

	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(final String medium) {
		this.medium = medium;
	}

	public String getOriginal() {
		return original;
	}

	public void setOriginal(final String original) {
		this.original = original;
	}
}
