package br.unicamp.ic.inf335.business;

import org.bson.types.ObjectId;

/**
 * POJO class that represents an Episode
 * 
 * @author Aruã Puertas Costa e Guilherme Kayo Shida
 * 
 */
public final class Episode {
	/**
	 * The ObjectId of the episode.
	 */
	private ObjectId object_id;
	/**
	 * The URL of the episode.
	 */
	private String url;
	/**
	 * The title of the episode.
	 */
	private String name;
	/**
	 * The season of the episode.
	 */
	private int season;
	/**
	 * The number of the episode.
	 */
	private int number;
	/**
	 * The date of the episode.
	 */
	private String airdate;
	/**
	 * The time of the episode.
	 */
	private String airtime;
	/**
	 * The timestamp of the episode.
	 */
	private String airstamp;
	/**
	 * The runtime of the episode.
	 */
	private double runtime;
	/**
	 * The image of the episode.
	 */
	private Image image;
	/**
	 * The summary of the episode.
	 */
	private String summary;

	public Episode() {

	}

	public ObjectId getObject_id() {
		return object_id;
	}

	public void setObject_id(final ObjectId object_id) {
		this.object_id = object_id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public int getSeason() {
		return season;
	}

	public void setSeason(final int season) {
		this.season = season;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(final int number) {
		this.number = number;
	}

	public String getAirdate() {
		return airdate;
	}

	public void setAirdate(final String airdate) {
		this.airdate = airdate;
	}

	public String getAirtime() {
		return airtime;
	}

	public void setAirtime(final String airtime) {
		this.airtime = airtime;
	}

	public String getAirstamp() {
		return airstamp;
	}

	public void setAirstamp(final String airstamp) {
		this.airstamp = airstamp;
	}

	public double getRuntime() {
		return runtime;
	}

	public void setRuntime(final double runtime) {
		this.runtime = runtime;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(final Image image) {
		this.image = image;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(final String summary) {
		this.summary = summary;
	}

	@Override
	public String toString() {
		StringBuilder message = new StringBuilder("E");
		message.append(this.number);
		message.append(": ");
		message.append(this.name);
		
		return message.toString();
	}
}
