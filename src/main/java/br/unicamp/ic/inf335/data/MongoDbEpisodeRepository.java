package br.unicamp.ic.inf335.data;

import static com.mongodb.client.model.Filters.eq;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import com.mongodb.MongoClientSettings;
import com.mongodb.MongoTimeoutException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import br.unicamp.ic.inf335.business.Episode;
import br.unicamp.ic.inf335.business.IEpisodeRepository;

/**
 * The repository that handles the operations with the episodes using Mongodb.
 * 
 * @author Aruã Puertas Costa e Guilherme Kayo Shida
 * 
 */
public class MongoDbEpisodeRepository implements IEpisodeRepository {
	/**
	 * The path to the mongodb database.
	 */
	private static final String MONGODB_PATH = "mongodb://localhost";
	/**
	 * The database name.
	 */
	private static final String MONGODB_DATABASE = "friends";
	/**
	 * The collection name.
	 */
	private static final String MONGODB_COLLECTION = "samples_friends";
	/**
	 * The Mongodb client.
	 */
	private MongoClient client;
	/**
	 * The codec that handles the deserialization of the Episode Class.
	 */
	private CodecRegistry pojoCodecRegistry;
	/**
	 * The Mongodb database.
	 */
	private MongoDatabase database;
	/**
	 * The Mongodb collection.
	 */
	private MongoCollection<Episode> collection;

	public MongoDbEpisodeRepository() {
		client = MongoClients.create(MONGODB_PATH);
		pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
				fromProviders(PojoCodecProvider.builder().automatic(true).build()));
		database = client.getDatabase(MONGODB_DATABASE).withCodecRegistry(pojoCodecRegistry);
		collection = database.getCollection(MONGODB_COLLECTION, Episode.class);
	}

	/**
	 * Get all episodes from the database.
	 * 
	 * @return
	 */
	@Override
	public List<Episode> getEpisodes() {
		List<Episode> list = new ArrayList<Episode>();

		try {
			System.out.println("Wait a moment, we are getting the episodes...");
			FindIterable<Episode> result = collection.find();

			result.forEach(new Consumer<Episode>() {
				@Override
				public void accept(Episode ep) {
					list.add(ep);
				}
			});

			return list;
		} finally {
			client.close();
		}
	}

	/**
	 * Get all episodes of a season from the database.
	 * 
	 * @param season the number of the season to get all episodes
	 */
	@Override
	public List<Episode> getEpisodesOfSeason(int season) {
		List<Episode> list = new ArrayList<Episode>();

		try {
			System.out.println("Wait a moment, we are getting the episodes...");
			FindIterable<Episode> result = collection.find(eq("season", season));

			if (result.cursor().hasNext()) {
				result.forEach(new Consumer<Episode>() {
					@Override
					public void accept(Episode ep) {
						list.add(ep);
					}
				});
			}
		} catch (MongoTimeoutException e) {
			System.out.println();
			System.out.println("***** ERROR! *****");
			System.out.println("We could not connect to the MongoDb database.\n" + "Check your database connection.");
		} finally {
			client.close();
		}

		return list;
	}
}
